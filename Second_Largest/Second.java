// Program to find Second largest when xyz are same, different or 2 same

public class Second{

    public static void main(String args[])
    {
        int x,y,z,val;
        x=10;
        y=20;
        z=30;

        val = FindSecondLargestNumber(x,y,z);
        System.out.println(val);
    }
    public static int FindSecondLargestNumber(int x,int y,int z)
    {
        int slar=0,lar=0,flag =0;
        
        if(x == y)
        {
            slar = lar = x;
            flag = 1;
        }
        else if(x > y)
        {
            slar = y;
            lar =x;
        }
        else
        {
            lar = y;
            slar = x;
        }
        if(z>slar && z> lar)
        {
            slar  = lar;
            lar = z;
        }
        else if(z>slar)
        {
            slar =z;
        }
        else if(flag == 1)
        {
            slar = z;
        }
        return slar;
    }
}