int FindSumLeavingOutRowCol(int** arr, int m, int n, int i,int j);

The function takes  a two-dimensional array 'arr', its number of rows 'm', its number of columns 'n' and integers 'i' and 'j' as input. Implement the function to find and return the sum of elements of the array leaving out the elements of the i and j column. The algorithm to find the sum is as follows:

1.Iterate over every row except for i row,and keep on adding each element except for the elements of j column to a variable 'sum'.
NOTE:
1.Row and column indices start from 0.
2. Value of i and j from 1.