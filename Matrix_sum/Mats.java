public class Mats{
    public static void main(String args[])
    {
        int[][] arr = {{1,2,3},{4,5,6},{7,8,9}};
        int m,n,i,j;
        m = n = 3;
        i=1;
        j=1;
        
        int sum = FindSumLeavingOutRowCol(arr,m,n,i,j);
        System.out.println(sum);
    }
    public static int FindSumLeavingOutRowCol(int[][] arr,int m,int n, int i, int j)
    {
        int sum=0;
        for(int p=0;p<m;p++)
        {
            for(int q=0;q<n;q++)
            {
                if(p == (i-1) || q == (j-1))
                continue;
                sum = sum + arr[p][q];
            }
        }
        return sum;
    }
}