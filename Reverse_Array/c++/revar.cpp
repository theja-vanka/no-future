#include<iostream>

using namespace std;


int* reverse(int *arr, int length)
{
    int i=0;
    int temp;
    int leng=length;
    while(i<length/2)
    {
        temp = arr[i];
        arr[i++]=arr[--leng];
        arr[leng]=temp;
    }
    return arr;
}


int main()
{
    int arr[5] = {10,20,30,40,50};
    int len = 5;
    reverse(arr,len);
    for(int x : arr)
    cout<< x << endl;
}

