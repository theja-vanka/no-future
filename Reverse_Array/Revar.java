public class Revar {

   public static void main(String args[]) {
      int [] numbers = {10, 20, 30, 40, 50};
      int length = 5;  
      
      numbers = ReversedArray(numbers,length);
      for(int x : numbers)
      {
          System.out.println(x);
      }
   }

   public static int[] ReversedArray (int[] arr, int length)
   {
       int temp;
       int i=0;
       int leng = length;
      while(i<length/2)
      {
          temp = arr[i];
          arr[i++]=arr[--leng];
          arr[leng]= temp;
      }
      return arr;
   }
   
}